import std.conv;
import std.format;
import std.stdio;
import std.string;
import std.traits;

import defs;

void main()
{
	auto f = stdout;
	foreach (lucky; [false, true])
	{
		f.writefln("[h2]Cumulative probabilities (%s \"Lucky\")[/h2]", lucky ? "with" : "without");
		f.writef("[table][tr][th]Outlook[/th]");
		foreach (r; investmentResults)
			f.writef("[th]%s[/th]", r.name.wrap(3).capitalize);
		f.writeln("[/tr]");

		foreach (level; outlookTemplates)
		{
			f.writef("[tr][td]%s[/td]", level.name);

			double[investmentResults.length] probs = 0;
			double remainingProb = 1.0;

			void doProb(size_t resultTier)
			{
				auto threshold = level.rolldownChance;
				auto stopProb = (threshold - 1) / 100.0;
				if (stopProb > 1)
					stopProb = 1;
				probs[resultTier] += stopProb * remainingProb;
				remainingProb *= 1 - stopProb;
			}
			doProb(0);
			doProb(1);
			doProb(2);
			doProb(3);
			if (lucky)
			{
				doProb(0);
				doProb(1);
				doProb(2);
				doProb(3);
			}
			probs[4] = remainingProb;

			foreach (i, investmentResult; investmentResults)
				f.writef("[td]%.2f%%[/td]",
					probs[i] * 100,
				);
			f.writeln("[/tr]");
		}
		f.writeln("[/table]");
	}
}
