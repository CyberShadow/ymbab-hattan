Mechanics :: Outcome Tier

The outcome tier is chosen based on the following algorithm:
[olist]
[*]Pick a number between 1 and 100 (inclusive). If it is below [b]Base Result Success %[/b], the resulting tier is "huge success". Otherwise, proceed to step 2.
[*]Pick a number between 1 and 100 (inclusive). If it is below [b]Base Result Success %[/b], the resulting tier is "better than expected". Otherwise, proceed to step 3.
[*]Pick a number between 1 and 100 (inclusive). If it is below [b]Base Result Success %[/b], the resulting tier is "exactly as planned". Otherwise, proceed to step 4.
[*]Pick a number between 1 and 100 (inclusive). If it is below [b]Base Result Success %[/b], the resulting tier is "not so well". Otherwise, proceed to step 5.
[*]If the player doesn't have the "Lucky" trait, the resulting tier is "total disaster". Otherwise, proceed to step 6.
   The "Lucky" trait is obtained by recruiting all monsters in the "Team Mana" group, including the secret Wraith monster.
[*]Pick a number between 1 and 100 (inclusive). If it is below [b]Base Result Success %[/b], the resulting tier is "huge success". Otherwise, proceed to step 7.
[*]Pick a number between 1 and 100 (inclusive). If it is below [b]Base Result Success %[/b], the resulting tier is "better than expected". Otherwise, proceed to step 8.
[*]Pick a number between 1 and 100 (inclusive). If it is below [b]Base Result Success %[/b], the resulting tier is "exactly as planned". Otherwise, proceed to step 9.
[*]Pick a number between 1 and 100 (inclusive). If it is below [b]Base Result Success %[/b], the resulting tier is "not so well". Otherwise, the resulting tier is "total disaster".
[/olist]

The final cumulative probabilities are as follows:

[h3]Cumulative probabilities (without "Lucky")[/h3]
[table][tr][th]Outlook[/th][th]Huge
success
[/th][th]Better
than
expected
[/th][th]Exactly
as
planned
[/th][th]Not
so
well
[/th][th]Total
disaster
[/th][/tr]
[tr][td]Optimistic[/td][td]44.00%[/td][td]24.64%[/td][td]13.80%[/td][td]7.73%[/td][td]9.83%[/td][/tr]
[tr][td]Great[/td][td]34.00%[/td][td]22.44%[/td][td]14.81%[/td][td]9.77%[/td][td]18.97%[/td][/tr]
[tr][td]Good[/td][td]29.00%[/td][td]20.59%[/td][td]14.62%[/td][td]10.38%[/td][td]25.41%[/td][/tr]
[tr][td]Stable[/td][td]59.00%[/td][td]24.19%[/td][td]9.92%[/td][td]4.07%[/td][td]2.83%[/td][/tr]
[tr][td]Risky[/td][td]24.00%[/td][td]18.24%[/td][td]13.86%[/td][td]10.54%[/td][td]33.36%[/td][/tr]
[tr][td]Volatile[/td][td]19.00%[/td][td]15.39%[/td][td]12.47%[/td][td]10.10%[/td][td]43.05%[/td][/tr]
[tr][td]Dangerous[/td][td]14.00%[/td][td]12.04%[/td][td]10.35%[/td][td]8.90%[/td][td]54.70%[/td][/tr]
[/table]

[h3]Cumulative probabilities (with "Lucky")[/h3]
[table][tr][th]Outlook[/th][th]Huge
success
[/th][th]Better
than
expected
[/th][th]Exactly
as
planned
[/th][th]Not
so
well
[/th][th]Total
disaster
[/th][/tr]
[tr][td]Optimistic[/td][td]48.33%[/td][td]27.06%[/td][td]15.16%[/td][td]8.49%[/td][td]0.97%[/td][/tr]
[tr][td]Great[/td][td]40.45%[/td][td]26.70%[/td][td]17.62%[/td][td]11.63%[/td][td]3.60%[/td][/tr]
[tr][td]Good[/td][td]36.37%[/td][td]25.82%[/td][td]18.33%[/td][td]13.02%[/td][td]6.46%[/td][/tr]
[tr][td]Stable[/td][td]60.67%[/td][td]24.87%[/td][td]10.20%[/td][td]4.18%[/td][td]0.08%[/td][/tr]
[tr][td]Risky[/td][td]32.01%[/td][td]24.33%[/td][td]18.49%[/td][td]14.05%[/td][td]11.13%[/td][/tr]
[tr][td]Volatile[/td][td]27.18%[/td][td]22.01%[/td][td]17.83%[/td][td]14.44%[/td][td]18.53%[/td][/tr]
[tr][td]Dangerous[/td][td]21.66%[/td][td]18.63%[/td][td]16.02%[/td][td]13.78%[/td][td]29.92%[/td][/tr]
[/table]
