import ae.utils.math;
import ae.utils.xmlbuild;

import std.algorithm.comparison;
import std.algorithm.iteration;
import std.conv;
import std.format;
import std.math;
import std.range;
import std.stdio;
import std.string;
import std.traits;

import defs;

enum marginT = 20;
enum marginB = 50;
enum marginL = 60;
enum marginR = 50;

enum svgW = 600;
enum svgH = marginT + maxAvgResult * pctScale + marginB;

enum minDistanceFactor = 0.6;
enum maxDistanceFactor = 1.8;

enum maxAvgResult = 250;

enum maxYield = 200; // 110 * 1.8 == 198
enum pctScale = double(svgW - marginL - marginR) / maxYield;

static immutable string[] outlookColors = [
	"#44FF44",
	"#00FF88",
	"#0088FF",
	"#0000FF",
	"#8800FF",
	"#FF0088",
	"#FF0000",
];

void main()
{
	foreach (lucky; [false, true])
	{
		auto svg = newXml().svg();
		svg.xmlns = "http://www.w3.org/2000/svg";
		svg["version"] = "1.1";
		svg.width  = text(svgW);
		svg.height = text(svgH);

		// svg.rect(["width" : svgW.text, "height" : svgH.text, "fill" : "black"]);

		// auto defs = svg.defs();
		auto bg = svg.g();
		auto grid = bg.path();
		auto axes = bg.g();
		auto texts = svg.g([
			"font-family" : "sans-serif",
			"font-weight" : "bold",
			"fill" : "white",
			"text-anchor" : "middle",
		]);
		// auto rowCaptions = texts.g([
		// 	"font-size" : "24pt",
		// ]);

		axes.path([
			"fill" : "none",
			"stroke" : "white",
			"stroke-width" :"4px",
			"d" :
				"M %s,%s v -%s l -5,10 h 10 l -5,-10 l -5,10".format(marginL, marginT + maxAvgResult * pctScale, maxAvgResult * pctScale)
				~ " " ~
				"M %s,%s h  %s l -10,-5 v 10 l 10,-5l -10,-5".format(marginL, marginT + maxAvgResult * pctScale, maxYield * pctScale         + 20)
		]);

		texts.text([
			"font-size" : "12pt",
			"transform" : "translate(%s,%s)".format(
				marginL,
				marginT + (maxAvgResult * pctScale) + marginB * 3 / 4,
			),
			"text-anchor" : "start",
		])[] = "SHOWN EST. RETURN";
		texts.text([
			"font-size" : "12pt",
			"transform" : "translate(%s,%s) rotate(-90)".format(
				15,
				marginT + (maxAvgResult * pctScale),
			),
			"text-anchor" : "start",
		])[] = "AVERAGE FINAL RETURN";
		string gridD;
		foreach (yield; iota(0, maxYield + 1, 20))
		{
			gridD ~= "M %s,%s v %s".format(
				marginL + yield * pctScale + 0.5,
				marginT,
				maxAvgResult * pctScale
			);
			texts.text([
				"font-size" : "10pt",
				"transform" : "translate(%s,%s)".format(
					marginL + yield * pctScale,
					marginT + (maxAvgResult * pctScale) + marginB * 1 / 3,
				),
			])[] = "%d%%".format(yield);
		}
		foreach (avgResult; iota(0, maxAvgResult + 1, 20))
		{
			gridD ~= "M %s,%s h %s".format(
				marginL,
				marginT + (maxAvgResult - avgResult) * pctScale - 0.5,
				maxYield * pctScale
			);
			texts.text([
				"font-size" : "10pt",
				"transform" : "translate(%s,%s)".format(
					marginL - 5,
					marginT + (maxAvgResult * pctScale) - (avgResult * pctScale),
				),
			"text-anchor" : "end",
			])[] = "%d%%".format(avgResult);
		}
		grid.d = gridD;
		grid.stroke = "#808080";
		// grid.stroke = "#ffffff80";
		// grid["stroke-width"] = "2";

		bg.path([
			"d" : "M %s,%s h %s".format(
				marginL,
				marginT + (maxAvgResult - 100) * pctScale - 0.5,
				maxYield * pctScale,
			),
			"stroke" : "#008000",
		]);

		// texts.text([
		// 	"font-size" : "12pt",
		// 	"transform" : "translate(%s,%s)".format(
		// 		margin,
		// 		margin + (maxScore * scoreScale) + 30,
		// 	),
		// ])[] = "0%";
		// texts.text([
		// 	"font-size" : "12pt",
		// 	"transform" : "translate(%s,%s)".format(
		// 		margin + probScale,
		// 		margin + (maxScore * scoreScale) + 30,
		// 	),
		// ])[] = "100%";

		enum legendX = marginL + maxYield     * pctScale * 0.8;
		enum legendY = marginT + maxAvgResult * pctScale * 0.6;
		enum legendW = 120;
		enum legendRowH = 20;
		enum legendMargin = 10;
		enum legendH = legendRowH * outlookTemplates.length + legendMargin * 2;
		bg.rect([
			"x" : text(legendX),
			"y" : text(legendY),
			"width" : text(legendW),
			"height" : text(legendH),
			"fill" : "black",
			"stroke" : "white",
			"stroke-width" : "3px",
		]);

		foreach (levelIndex, level; outlookTemplates)
		{
			double[investmentResults.length] probs = 0;
			double remainingProb = 1.0;

			void doProb(size_t resultTier)
			{
				auto threshold = level.rolldownChance;
				auto stopProb = (threshold - 1) / 100.0;
				if (stopProb > 1)
					stopProb = 1;
				probs[resultTier] += stopProb * remainingProb;
				remainingProb *= 1 - stopProb;
			}
			doProb(0);
			doProb(1);
			doProb(2);
			doProb(3);
			if (lucky)
			{
				doProb(0);
				doProb(1);
				doProb(2);
				doProb(3);
			}
			probs[4] = remainingProb;

			string d;

			auto minRolledYield = cast(int)floor(level. lowYield * minDistanceFactor);
			auto maxRolledYield = cast(int)ceil (level.highYield * maxDistanceFactor);
			foreach (rolledYield; [minRolledYield, maxRolledYield])
			{
				double avgResult = 0;
				foreach (i, investmentResult; investmentResults)
					avgResult += probs[i] * (investmentResult.multiplier.isNaN ? 0 : (100 + (rolledYield * investmentResult.multiplier)));
				assert(avgResult <= maxAvgResult);
				d ~= "%s%s,%s".format(
					(d.length ? " L " : "M "),
					marginL + rolledYield * pctScale,
					marginT + (maxAvgResult * pctScale) - (avgResult * pctScale),
				);
			}

			svg.path([
				"d" : d,
				"fill" : "none",
				"stroke" : outlookColors[levelIndex],
				"stroke-width" : "3px",
			]);

			svg.rect([
				"x"      : text(legendX + legendMargin),
				"y"      : text(legendY + legendMargin + levelIndex * legendRowH),
				"width"  : text(16),
				"height" : text(16),
				"fill"   : outlookColors[levelIndex],
			]);
			texts.text([
				"transform" : "translate(%s,%s)".format(
					legendX + legendMargin + 20,
					legendY + legendMargin + levelIndex * legendRowH + 13,
				),
				"fill" : "white",
				"text-anchor" : "start",
			])[] = level.name;
			
		}

		svg.toPrettyString().toFile("probs-%d.svg".format(lucky));
	}
}

// "
