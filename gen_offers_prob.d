import std.stdio;

void main()
{
	double remainingProb = 1.0;
	for (int offersMade = 0; ; offersMade++)
	{
		float threshold = 2f ^^ offersMade;
		// bool stop = uniform(1, 101) < threshold;
		auto stopProb = (threshold - 1) / 100.0;
		if (stopProb > 1)
			stopProb = 1;

		writefln("[tr][td]%d[/td][td]%.2f%%[/td][/tr]",
			offersMade + 1,
			stopProb * remainingProb * 100,
		);
		remainingProb *= (1 - stopProb);
		if (remainingProb <= 0)
			break;
	}
}
