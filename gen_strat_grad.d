import ae.utils.math;
import ae.utils.xmlbuild;

import std.algorithm.comparison;
import std.conv;
import std.format;
import std.math;
import std.range;
import std.stdio;
import std.string;
import std.traits;

import defs;

enum marginT = 50;
enum marginB = 75;
enum marginL = 50;
enum marginR = 50;

enum svgW = 600;
enum rowH = 100;
enum svgH = marginT + outlookTemplates.length * rowH + marginB;

enum minDistanceFactor = 0.6;
enum maxDistanceFactor = 1.8;

enum minAvgResult =   0;
enum maxAvgResult = 180;
enum minAvgResultHue =   0;
enum maxAvgResultHue = 200;

enum maxYield = 200; // 110 * 1.8 == 198
enum yieldFactor = double(svgW - marginL - marginR) / maxYield;

void main()
{
	foreach (lucky; [false, true])
	{
		auto svg = newXml().svg();
		svg.xmlns = "http://www.w3.org/2000/svg";
		svg["version"] = "1.1";
		svg.width  = text(svgW);
		svg.height = text(svgH);

		svg.rect(["width" : svgW.text, "height" : svgH.text, "fill" : "black"]);

		svg.path([
			"fill" : "none",
			"stroke" : "white",
			"stroke-width" :"4px",
			"d" :
				"M %s,%s v -%s"                              .format(marginL, marginT + outlookTemplates.length * rowH, outlookTemplates.length * rowH)
				~ " " ~
				"M %s,%s h  %s l -10,-5 v 10 l 10,-5l -10,-5".format(marginL, marginT + outlookTemplates.length * rowH, maxYield * yieldFactor         + 20)
		]);

		auto defs = svg.defs();
		auto grid = svg.g();
		auto texts = svg.g([
			"font-family" : "sans-serif",
			"font-weight" : "bold",
			"fill" : "white",
			"text-anchor" : "middle",
		]);
		auto rowCaptions = texts.g([
			"font-size" : "24pt",
		]);

		texts.text([
			"font-size" : "12pt",
			"transform" : "translate(%s,%s)".format(
				marginL,
				marginT + (outlookTemplates.length * rowH) + marginB * 2 / 4,
			),
			"text-anchor" : "start",
		])[] = "SHOWN EST. RETURN";
		foreach (yield; iota(0, maxYield + 1, 20))
		{
			grid.path([
				"d" : "M %s,%s v %s".format(
					marginL + yield * yieldFactor + 0.5,
					marginT, outlookTemplates.length * rowH),
				"stroke" : "#ffffff80",
			]);
			texts.text([
				"font-size" : "10pt",
				"transform" : "translate(%s,%s)".format(
					marginL + yield * yieldFactor,
					marginT + (outlookTemplates.length * rowH) + marginB * 1 / 4,
				),
			])[] = "%d%%".format(yield);
		}

		// texts.text([
		// 	"font-size" : "12pt",
		// 	"transform" : "translate(%s,%s)".format(
		// 		margin,
		// 		margin + (maxScore * scoreScale) + 30,
		// 	),
		// ])[] = "0%";
		// texts.text([
		// 	"font-size" : "12pt",
		// 	"transform" : "translate(%s,%s)".format(
		// 		margin + probScale,
		// 		margin + (maxScore * scoreScale) + 30,
		// 	),
		// ])[] = "100%";

		foreach (levelIndex, level; outlookTemplates)
		{
			double[investmentResults.length] probs = 0;
			double remainingProb = 1.0;

			void doProb(size_t resultTier)
			{
				auto threshold = level.rolldownChance;
				auto stopProb = (threshold - 1) / 100.0;
				if (stopProb > 1)
					stopProb = 1;
				probs[resultTier] += stopProb * remainingProb;
				remainingProb *= 1 - stopProb;
			}
			doProb(0);
			doProb(1);
			doProb(2);
			doProb(3);
			if (lucky)
			{
				doProb(0);
				doProb(1);
				doProb(2);
				doProb(3);
			}
			probs[4] = remainingProb;

			auto grad = defs.linearGradient();
			grad.id = "res%d".format(levelIndex);
			grad.x1 = "0%";
			grad.y1 = "0%";
			grad.x2 = "100%";
			grad.y2 = "0%";

			auto minRolledYield = cast(int)floor(level. lowYield * minDistanceFactor);
			auto maxRolledYield = cast(int)ceil (level.highYield * maxDistanceFactor);
			foreach (rolledYield; minRolledYield .. maxRolledYield + 1)
			{
				double avgResult = 0;
				foreach (i, investmentResult; investmentResults)
					avgResult += probs[i] * (investmentResult.multiplier.isNaN ? 0 : 1 + (rolledYield * investmentResult.multiplier));
				auto offset = (rolledYield - minRolledYield) * 1.0 / (maxRolledYield - minRolledYield);
				assert(avgResult <= maxAvgResult);
				auto h = itpl(minAvgResultHue, maxAvgResultHue,  avgResult, minAvgResult, maxAvgResult);
				grad.stop(["offset" : format("%s%%", offset * 100), "style" : format("stop-color:hsl(%s,100%%,50%%)", h)]);
			}

			svg.rect([
				"x"      : text(marginL + minRolledYield * yieldFactor),
				"y"      : text(marginT + levelIndex * rowH + rowH / 2),
				"width"  : text((maxRolledYield - minRolledYield) * yieldFactor),
				"height" : text(rowH / 2),
				"fill"   : "url('#res%d')".format(levelIndex),
			]);

			rowCaptions.text([
				"transform" : "translate(%s,%s)".format(
					marginL + max(60, (minRolledYield + maxRolledYield) / 2 * yieldFactor),
					marginT + levelIndex * rowH + rowH * 7 / 16,
				),
				"fill" : level.color,
			])[] = level.name;
			
		}

		svg.toPrettyString().toFile("probs-%d.svg".format(lucky));
	}
}

// "
