struct OutlookLevel
{
	string name, color;
	int lowYield, highYield, rolldownChance;
}
static immutable OutlookLevel[] outlookTemplates = [
	{ "Optimistic", "#00FF00", 20,  40, 45 },
	{ "Great"     , "#00FF00", 15,  30, 35 },
	{ "Good"      , "#0000FF", 15,  30, 30 },
	{ "Stable"    , "#0000FF", 10,  20, 60 },
	{ "Risky"     , "#FF00FF", 40,  60, 25 },
	{ "Volatile"  , "#FF00FF", 50,  80, 20 },
	{ "Dangerous" , "#FF0000", 80, 110, 15 },
];

struct InvestmentResult
{
	string name;
	double multiplier;
}
static immutable InvestmentResult[] investmentResults = [
	{ "huge success"        , 1.8 },
	{ "better than expected", 1.3 },
	{ "exactly as planned"  , 1.0 },
	{ "not so well"         , 0.5 },
	{ "total disaster"      , double.nan },
];
